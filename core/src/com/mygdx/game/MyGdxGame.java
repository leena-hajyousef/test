package com.mygdx.game;


import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.ArrayList;

import static java.security.AccessController.getContext;

public class MyGdxGame extends ApplicationAdapter {
	ArrayList<ImageButton> element = new ArrayList<ImageButton>();

	Texture myTexture;
	TextureRegion myTextureRegion;
	TextureRegionDrawable myTexRegionDrawable ;
	ImageButton button;

	Texture myTexture1;
	TextureRegion myTextureRegion1;
	TextureRegionDrawable myTexRegionDrawable1 ;
	ImageButton button1;

	Texture myTexture2;
	TextureRegion myTextureRegion2;
	TextureRegionDrawable myTexRegionDrawable2 ;
	ImageButton button2;
	Texture myTexture3;
	TextureRegion myTextureRegion3;
	TextureRegionDrawable myTexRegionDrawable3;
	ImageButton button3;
	Texture myTexture4;
	TextureRegion myTextureRegion4;
	TextureRegionDrawable myTexRegionDrawable4;
	ImageButton button4;
	SpriteBatch batch;
	Texture img;
	Texture bg;
	Stage stage;
	Skin skin;

	Label outputLabel;
	Image image;

	ShapeRenderer shapeRenderer ;
	boolean drawLine = false;
	private float stageWidth;
	private float stageHeight;

	@Override
	public void create () {
		element.add(button);
		element.add(button1);
		element.add(button2);
		element.add(button3);
		element.add(button4);
		shapeRenderer = new ShapeRenderer();
		batch = new SpriteBatch();
		img = new Texture("badlogic.jpg");
		 bg = new Texture(Gdx.files.internal("funky-lines.png"));

		//camera = new OrthographicCamera();
		int lineWidth = 8; // pixels

		stage = new Stage(new ScreenViewport());
		Gdx.input.setInputProcessor(stage);
		//controller = new MyController();
	//	Gdx.input.setInputProcessor(controller);
		stageHeight = Gdx.graphics.getHeight();
		stageWidth = Gdx.graphics.getWidth();
		skin = new Skin(Gdx.files.internal("uiskin.json"));
		// Background
		Texture board = new Texture(Gdx.files.internal("funky-lines.png"));
		Image background = new Image(board);
		background.setOrigin(0, 0);
		background.setSize(stageWidth, stageHeight);
		background.rotateBy(0);
		background.setPosition(0, 0);

		//title
		Label title = new Label("Buttons with Skins",skin);
		title.setAlignment(Align.center);
		title.setPosition(Gdx.graphics.getWidth()/2-100, Gdx.graphics.getHeight()-100);
		//stage.addActor(background);
		stage.addActor(title);
		Table table = new Table();
		table.setDebug(true);
		table.setFillParent(true);
		myTexture = new Texture(Gdx.files.internal("triangle-512_grande.png"));
		myTextureRegion = new TextureRegion(myTexture);
		myTexRegionDrawable = new TextureRegionDrawable(myTextureRegion);
		button = new ImageButton(myTexRegionDrawable);
		button.setPosition(250, Gdx.graphics.getHeight()-250);
		button.setSize(100,100);
		stage.addActor(button);
		myTexture1 = new Texture(Gdx.files.internal("images.png"));
		myTextureRegion1 = new TextureRegion(myTexture1);
		myTexRegionDrawable1 = new TextureRegionDrawable(myTextureRegion1);
		button1 = new ImageButton(myTexRegionDrawable1);
		button1.setPosition(250, Gdx.graphics.getHeight()-450);
		button1.setSize(100,100);
		button1.addListener(new InputListener(){
			@Override
			public void enter(InputEvent event, float x, float y,
							  int pointer, Actor fromActor) {



			}
			@Override
			public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
				//Gdx.app.log("MyTag", "touchup");


			}
			@Override
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				Gdx.app.log("Example", "touch  started at (" + x + ", " + y + ")");
				return true;
			}
		});
		stage.addActor(button1);
		myTexture2 = new Texture(Gdx.files.internal("images.png"));
		myTextureRegion2 = new TextureRegion(myTexture2);
		myTexRegionDrawable2 = new TextureRegionDrawable(myTextureRegion2);
		button2 = new ImageButton(myTexRegionDrawable2);
		button2.setPosition(250, Gdx.graphics.getHeight()-650);
		button2.setSize(100,100);
		stage.addActor(button2);

		myTexture3 = new Texture(Gdx.files.internal("images.png"));
		myTextureRegion3= new TextureRegion(myTexture3);
		myTexRegionDrawable3 = new TextureRegionDrawable(myTextureRegion3);
		button3= new ImageButton(myTexRegionDrawable3);
		button3.setPosition(850, Gdx.graphics.getHeight()-250);
		button3.setSize(100,100);
		button3.setDebug(true);
		button1.setDebug(true);
		button.setDebug(true);
		button2.setDebug(true);

		stage.addActor(button3);
		myTexture4 = new Texture(Gdx.files.internal("triangle-512_grande.png"));
		myTextureRegion4= new TextureRegion(myTexture4);
		myTexRegionDrawable4= new TextureRegionDrawable(myTextureRegion4);
		button4= new ImageButton(myTexRegionDrawable4);
		button4.setPosition(850, Gdx.graphics.getHeight()-450);
		button4.setSize(100,100);
		button4.setDebug(true);
		button4.addListener(new InputListener(){
			@Override
			public void enter(InputEvent event, float x, float y,
							  int pointer, Actor fromActor) {

				drawLine=true;

			}
			@Override
			public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	//Gdx.app.log("MyTag", "touchup");


			}
			@Override
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				Gdx.app.log("MyTag", "touchdown");
				return true;
			}
		});
		stage.addActor(button4);

	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);




		if (drawLine) {
			if(Gdx.input.isTouched())
			{
				shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
				shapeRenderer.setColor(Color.BLACK);
				float x1 = Gdx.input.getX();
				float y1 = Gdx.input.getY();
				Gdx.app.log("MyTag", String.valueOf(x1));
				Gdx.app.log("MyTag", String.valueOf(y1));
				shapeRenderer.line(850, Gdx.graphics.getHeight()-450,x1 ,stageHeight-y1);
				shapeRenderer.end();
			}


		}

		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();

	}

	@Override
	public void dispose () {
		batch.dispose();
		img.dispose();
		stage.dispose();
	}
}

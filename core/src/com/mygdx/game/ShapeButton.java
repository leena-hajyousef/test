package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

/**
 * Created by khalk on 4/11/2018.
 */

public class ShapeButton extends ImageButton {
    private  int id;
    private boolean selected;
    private String side;


    public void setId(int id) {
        this.id = id;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public int getId() {
        return id;
    }

    public boolean isSelected() {
        return selected;
    }

    public String getSide() {
        return side;
    }

    public ShapeButton(Skin skin) {
        super(skin);
    }

    public ShapeButton(Skin skin, String styleName) {
        super(skin, styleName);
    }

    public ShapeButton(ImageButtonStyle style) {
        super(style);
    }

    public ShapeButton(Drawable imageUp) {
        super(imageUp);
    }

    public ShapeButton(Drawable imageUp, Drawable imageDown) {
        super(imageUp, imageDown);
    }

    public ShapeButton(Drawable imageUp, Drawable imageDown, Drawable imageChecked) {
        super(imageUp, imageDown, imageChecked);
    }

}
